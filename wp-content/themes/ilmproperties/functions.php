<?php

// widgetized sidebar

if ( function_exists('register_sidebar') )
register_sidebar(array(
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
));

// Get page by slug function

function get_ID_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

function my_excerpt_length($length) {
    return 30;
}
add_filter('excerpt_length', 'my_excerpt_length');


    $lang = TEMPLATE_PATH . '/lang';
    load_theme_textdomain('carkod', $lang);

// camera slideshow
if (function_exists('camera_main_ss_add')) {
    add_action('admin_init','camera_main_ss_add');
}


// Post thumbnail

if ( function_exists( 'add_theme_support' ) ) { 
add_theme_support( 'post-thumbnails' );

// additional image sizes
// delete the next line if you do not need additional image sizes
}

// Post types

  
  add_action( 'init', 'create_post_type' );
function create_post_type() {
  
  register_post_type('footerpage', array(
'label' => 'Pie de Página',
'public' => true,
'show_ui' => true,
'capability_type' => 'page',
'hierarchical' => false,
'rewrite' => array('slug' => 'footerpage'),
'query_var' => true,
'menu_position' => 5,
'supports' => array(
'title',
'editor',
'custom-fields',
'revisions',
'page-attributes',)
) );


register_post_type('wikiciudades', array(
'label' => 'Ciudades',
'public' => true,
'show_ui' => true,
'capability_type' => 'page',
'hierarchical' => false,
'query_var' => true,
'menu_position' => 6,
'show_in_nav_menus' => true,
'can_export' => true,
'taxonomies' => array('category', 'post_tag'), // this is IMPORTANT
'supports' => array(
'title',
'editor',
'revisions',)

) );
}

// End Post types


// Register Taxonomies

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'ciudades', 0 );

//create a custom taxonomy name it topics for your posts

function ciudades() {

// Add new taxonomy, make it hierarchical like categories
// first do the translations part for GUI

  $ciudades = array(
    'name' => _x( 'Ciudades', 'taxonomy general name' ),
    'singular_name' => _x( 'Ciudad', 'taxonomy singular name' ),
    'menu_name' => __( 'Todas las Ciudades' ),
  ); 	

// Now register the taxonomy

  register_taxonomy('ciudades',array('post'), array(
    'hierarchical' => true,
    'labels' => $ciudades,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'ciudad' ),
  ));

}

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'precios', 0 );

//create a custom taxonomy name it topics for your posts

function precios() {

// Add new taxonomy, make it hierarchical like categories
// first do the translations part for GUI

  $precios = array(
    'name' => _x( 'Precios', 'taxonomy general name' ),
    'singular_name' => _x( 'Precio', 'taxonomy singular name' ),
    'menu_name' => __( 'Todos los precios' ),
  ); 	

// Now register the taxonomy

  register_taxonomy('precios',array('post'), array(
    'hierarchical' => true,
    'labels' => $precios,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'precio' ),
  ));

}

// end Taxonomies reg


// Meta boxes for easy admin

add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cmb_';

	$meta_boxes[] = array(
		'id'         => 'info',
		'title'      => 'Información Propiedad',
		'pages'      => array( 'post', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			
			 array(
             'name' => 'DETALLES DE LA PROPIEDAD',
             'desc' => 'RELLENE LOS SIGUIENTES CAMPOS CON DATOS BÁSICOS DE LA PROPIEDAD PREFERIBLEMENTE SIN ESPACIOS Y CON UNA PALABRA EN LOS CAMPOS CORRESPONDIENTES A PALABRAS',
             'id'   => $prefix . 'detalles',
             'type' => 'title',
                        ),
			array(
				'name' => 'Referencia',
				'desc' => 'Referencia ILM',
				'id'   => $prefix . 'referencia',
				'type' => 'text',
			),
		
			array(
				'name' => 'Tipo',
				'desc' => 'Tipo de propiedad',
				'id'   => $prefix . 'tipo',
				'type' => 'text_small',
			),
			
			array(
				'name' => 'Superficie Vivienda',
				'desc' => 'm&sup2;',
				'id'   => $prefix . 'superficiev',
				'type' => 'text_small',
			),
		
			array(
                                'name' => 'Superficie Parcela',
                                'desc' => 'm&sup2;',
                                'id'   => $prefix . 'superficiep',
                                'type' => 'text_small',
                        ),
			
			array(
				'name' => 'Dormitorios',
				'desc' => 'Número de Dormitorios',
				'id'   => $prefix . 'dormitorios',
				'type' => 'text_small',
			),
			array(
				'name' => 'Baños',
				'desc' => 'Número de baños',
				'id'   => $prefix . 'banos',
				'type' => 'text_small',
			),

			array(
				'name'   => 'Precio',
				'desc'   => '',
				'id'     => $prefix . 'precio',
				'before' => ' ',
				'after' => '€',
				'type'   => 'text_money',
				
			),
		
						
		
			 array(
                                'name' => 'DESCRIPCION EXTENSIVA DE LA PROPIEDAD',
                                'desc' => 'ESCRIBA UNA BREVE DESCRIPCION SOBRE LA PROPIEDAD. PUEDE INCLUIR NEGRITA, SUBRAYADO, TACHADO, CURSIVA, LINKS EXTERNOS, LISTAS, COLORES, CITAS. EN CASO DE QUERER INCLUIR FOTOS CONSULTAR AL ADMINISTRADOR.',
                                'id'   => $prefix . 'detalles',
                                'type' => 'title',
                        ),
			
			
			array(
	'name' => 'Texto descripción',
	'desc' => 'Descripción de la propiedad',
	'id' => $prefix . 'descripcion',
	'type' => 'wysiwyg',
),
			array(
				'name' => 'Foto de Reclamo',
				'desc' => 'Subir foto menos de 2MB',
				'id'   => $prefix . 'test_image',
				'type' => 'file',
			),
			array(
				'name' => 'oEmbed',
				'desc' => 'Introduce un vídeo de youtube, twitter, or instagram.',
				'id'   => $prefix . 'test_embed',
				'type' => 'oembed',
			),
		),
	);

$meta_boxes[] = array(
		'id'         => 'wiki',
		'title'      => 'CONTENIDO ADICIONAL: FOTOS PARA EL CARRUSEL Y MAPA CON LA CIUDAD',
		'pages'      => array( 'wikiciudades', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			
			
			array(
				'name'    => __( 'Fotos Carrusel de la ciudad', 'cmb' ),
				'desc'    => __( 'Se recomienda no subir más 5 fotos o archivos superiores a 2MB', 'cmb' ),
				'id'      => 'carrusel',
				'type'    => 'wysiwyg',
				'options' => array( 'textarea_rows' => 20, ),
			),
			array(
				'name'    => __( 'Mapa con la ciudad', 'cmb' ),
				'desc'    => __( 'Subir sólo el mapa prediseñado', 'cmb' ),
				'id'      => 'mapa',
				'type'    => 'wysiwyg',
				'options' => array( 'textarea_rows' => 10, ),
			),
		),
	);



	// Add other metaboxes as needed

	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'init.php';

}


// Taxnomy search

function buildSelect($tax){	
	$terms = get_terms($tax);
	$x = '<select name="'. $tax .'">';
	$x .= '<option value="">Select '. ucfirst($tax) .'</option>';
	foreach ($terms as $term) {
	   $x .= '<option value="' . $term->slug . '">' . $term->name . '</option>';	
	}
	$x .= '</select>';
	return $x;
}


$args['post_type'] = 'post';
$args['showposts'] = 100;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args['paged'] = $paged;  
$args['tax_query'] = $cleanArray; 
$the_query = new WP_Query( $args );

// Theme localization

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('ilmproperties', get_template_directory() . '/languages');
}


// Query multiple taxonomies base url change


// hook the translation filters, changes the names
add_filter(  'gettext',  'change_post_to_article'  );
add_filter(  'ngettext',  'change_post_to_article'  );

function change_post_to_article( $translated ) {
     $translated = str_ireplace(  'Post',  'Properties',  $translated );  // ireplace is PHP5 only
	 $translated = str_ireplace(  'Entradas',  'Propiedades',  $translated );  // ireplace is PHP5 only
	 $translated = str_ireplace(  '文章',  '不动产',  $translated );  // ireplace is PHP5 only
 	 $translated = str_ireplace(  'Uncategorized',  'Todas',  $translated );  // ireplace is PHP5 only

     return $translated;
}

// Avoid canonical URL redirect

remove_filter('template_redirect', 'redirect_canonical');

?>