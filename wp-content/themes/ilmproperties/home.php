<?php get_header(); ?>


<script type="text/javascript">
jQuery(document).ready(function () {
	
	$('.jcarousel')
	
    .jcarousel({
       wrap: 'circular',
    })
    .jcarouselAutoscroll({
        interval: 3000,
        target: '+=1',
        autostart: true
    });
	
	$(this).find("img").captionjs({
        'class_name' : 'captionjs',  // Class name assigned to each <figure>
        
        'mode'       : 'default',    // default | static | animated | hide
    });
	
	});
</script>

<div class="jcarousel">
<?php query_posts( 'posts_per_page=-1&order=RAND' ); if (have_posts()) :  ?>

<div class="jcarousel-container">
<?php while (have_posts()) : ?> 


<?php the_post(); ?>
    	<?php if( function_exists( 'catch_that_image' ) ) { ; ?>
		<a href="<?php the_permalink (); ?>" title="<?php _e('Ver esta Propiedad'); ?>"><img src="<?php echo catch_that_image() ; ?>" width="897" height="493" alt="<?php the_title (); echo ', ';  $terms = get_the_terms( $post->ID, 'ciudades' ); if ( $terms && ! is_wp_error( $terms ) ) : $ciudades_links = array();foreach ( $terms as $term ) {$ciudades_links[] = $term->name;}	$on_ciudades = join( ", ", $ciudades_links ); echo $on_ciudades; ?>"/><?php endif ; ?></a>
    	<?php } else {echo '¡Error! Contacte con el administrador';} ?>	
		
		<?php endwhile; ?>        
</div>    	       
	<?php endif; ?>
    <div style="clear:both;"></div>

    <div class="more"><a href="<?php bloginfo('url') ; ?>/?cat=1"><?php _e('Ver mas propiedades'); ?></a></div>

</div>
<?php //end of container1 ?> 
<?php get_footer(); ?>

