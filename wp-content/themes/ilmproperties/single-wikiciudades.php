<?php get_header(); ?>

<script type="text/javascript">
$(document).ready(function() {
		//Thumbnailer.config.shaderOpacity = 1;
		var tn1 = $('.mygallery').tn3({
			
			content:[{
    plugin: "mediaelement",
    options: {
        features: ['current','progress','duration','fullscreen'],
        useFullScreen: true
    }
}],
autoplay: true,
delay:3000,
imageClick:"fullscreen",
image:{
maxZoom:2,
crop:true,
clickEvent:"dblclick",
transitions:[{
type:"blinds"
},{
type:"grid"
},{
type:"grid",
duration:460,
easing:"easeInQuad",
gridX:1,
gridY:8,
// flat, diagonal, circle, random
sort:"random",
sortReverse:false,
diagonalStart:"bl",
// fade, scale
method:"scale",
partDuration:360,
partEasing:"easeOutSine",
partDirection:"left" ,
}]}

});

$( "#mapa" ).dialog({
      autoOpen: false,
	  height: 450,
    width: 550,
    modal: true,
    resizable: true,
    dialogClass: 'meinteresa-modal' ,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
		
		$("a.link-mapa").click(function(event){
			event.preventDefault();
			$("#mapa").dialog("open");
			
			
			});



	});
</script>

<style type="text/css">

div.tn3-controls-bg, div.tn3_rh, div.tn3-thumbs, div.tn3-play , div.tn3-fullscreen, div.tn3-next-page, div.tn3-prev-page, div.tn3-count { display:none;}
#page .mygallery { width:930px;}
.tn3-gallery {
    position: relative;
    width: 800px;
    height: 400px;
    background-color: #000000;
    line-height: normal;
	margin: 50px;
}
.tn3-gallery img {
    max-width: none !important;
}    
.tn3-image {
    position: absolute;
    width: 840px;
    height: 400px;
    background-color: #000000;
}

</style>
<?php if ( have_posts() ) :  ?>
<div id="page">


<?php while ( have_posts() ) : the_post(); ?>



		<h2 class="page-title"><?php the_title (); ?><a href="#mapa" class="link-mapa"><?php _e('Ver Mapa'); ?></a></h2>	
        <div class="mapa" id="mapa"><?php echo get_post_meta( get_the_ID(), 'mapa', true );  ?></div>
        <div class="mygallery">
        <?php echo get_post_meta( get_the_ID(), 'carrusel', true );?>			
        </div>
		<?php the_content (); ?>
            
		<?php endwhile; ?> 
        
</div>
    <?php else: ?>
		<p><?php _e('Sorry, I could not find what you are looking for.'); ?></p>
  
	<?php endif; ?>

      

<?php get_footer(); ?>

