<?php get_header(); ?>
<div id="page">
<?php if ( have_posts() ) :  ?>

<?php while ( have_posts() ) : the_post(); ?>



		<h2 class="page-title"><?php the_title (); ?></h2>					
		<?php the_content (); ?>
            
		<?php endwhile; ?> 
        

    <?php else: ?>
		<p><?php _e('Sorry, I could not find what you are looking for.'); ?></p>

      
	<?php endif; ?>


</div>  
<?php get_footer(); ?>

