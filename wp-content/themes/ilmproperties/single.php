<?php get_header(); ?>

<script type="text/javascript">

$(document).ready(function(){
    $('a.atras').click(function(){
        parent.history.back();
        return false;
    });
});

	$(document).ready(function() {
		//Thumbnailer.config.shaderOpacity = 1;
		var tn1 = $('.mygallery').tn3({
			
			content:[{
    plugin: "mediaelement",
    options: {
        features: ['current','progress','duration','fullscreen'],
        useFullScreen: true
    }
}],

imageClick:"fullscreen",
image:{
maxZoom:1.5,
crop:false,
clickEvent:"dblclick",
transitions:[{
type:"blinds"
},{
type:"grid"
},{
type:"grid",
duration:460,
easing:"easeInQuad",
gridX:1,
gridY:8,
// flat, diagonal, circle, random
sort:"random",
sortReverse:false,
diagonalStart:"bl",
// fade, scale
method:"scale",
partDuration:360,
partEasing:"easeOutSine",
partDirection:"left" ,
}]}




});
		
		$( "#meinteresa-form" ).dialog({
      autoOpen: false,
	  height: 450,
    width: 450,
    modal: true,
    resizable: true,
    dialogClass: 'meinteresa-modal' ,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
		
		$("#meinteresa a").click(function(event){
			event.preventDefault();
			$("#meinteresa-form").dialog("open");
			
			
			});
			
		
		
		
	});
</script>

<div id="single">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post (); ?>

	<div class="single-navigation">
    	<div class="back">
    	<a class="atras" title="<?php _e('Hacia Atrás'); ?>" ><?php _e('&laquo; Atrás'); ?></a>
        </div>
        <div class="post-links">
        <?php previous_post_link('%link', '&lt; Anterior ', TRUE); next_post_link('%link', ' Siguiente &gt;', TRUE) ?>
        </div>
    	
    </div>
	<div class="topinfo">

		<div id="lugar">
    		<?php $terms = get_the_terms( $post->ID, 'ciudades' );					
    if ( $terms && ! is_wp_error( $terms ) ) : $ciudades_links = array();foreach ( $terms as $term ) {$ciudades_links[] = $term->name;}	$on_ciudades = join( ", ", $ciudades_links ); echo $on_ciudades; endif; ?>
    	</div>
    
    
    	<div id="precio">
    		<?php global $post; echo get_post_meta( get_the_ID(), '_cmb_precio', true ); echo '&nbsp;€'; ?>
    	</div>

	</div>

	<div class="content">
    
    <div class="top-content">
		<div class="mygallery">
			<div id="tn3 album">
				<?php the_content(); ?>
    		</div>
		</div>

	
  

			<div id="metabox1">
            	<h2 class="metabox1-title"><?php _e('DETALLES DE LA PROPIEDAD'); ?></h2>
                
<p><span class="general"><?php _e('REFERENCIA:&nbsp;') ; ?><?php the_title() ;?></span><br /></p>
<p><span class="general"><?php _e('TIPO:&nbsp;') ; ?><?php global $post; echo get_post_meta( get_the_ID(), '_cmb_tipo', true );?></span><br /></p>
<p><span class="general"><?php _e('SUPERFICIE VIVIENDA:&nbsp;') ; ?> <?php global $post; echo get_post_meta( get_the_ID(), '_cmb_superficiev', true ); echo ' m&sup2;' ;?></span><br /></p>
<p><span class="general"><?php _e('SUPERFICIE PARCELA&nbsp;:') ; ?> <?php global $post; echo get_post_meta( get_the_ID(), '_cmb_superficiep', true );  echo ' m&sup2;' ?></span><br /></p>
<p><span class="general"><?php _e('DORMITORIOS:&nbsp;') ; ?> <?php global $post; echo get_post_meta( get_the_ID(), '_cmb_dormitorios', true ); ?></span><br /></p>
<p><span class="general"><?php _e('BA&Ntildeos:&nbsp;') ; ?> <?php  global $post; echo get_post_meta( get_the_ID(), '_cmb_banos', true );?></span><br /></p>
<p><span class="general">CIUDAD:&nbsp;<a class="wikiciudad-link" href="<?php $terms_as_text = get_the_term_list( $post->ID, 'ciudades', '', ', ', '' ) ; bloginfo ('url') ; echo '/?wikiciudades=' ; echo strip_tags($terms_as_text); ?>"><?php echo strip_tags($terms_as_text); ?></a></span></p>
				



			<div id="meinteresa"><a href="<?php $contacto = get_page_by_title( 'Contacto' ); the_permalink($contacto); ?>">Me interesa</a></div>
        
        <script type="text/javascript">
		$(document).ready(function(){
		var property = "<?php the_title();?>" ;
		$("#meinteresa-form").find("textarea").text(function () {
    return $(this).text().replace("$property", property); 
});
	});
		</script>
        
        
        <div id="meinteresa-form"><?php  echo do_shortcode( '[contact-form-7 id="9" title="Formulario de contacto 1"]' ); ?></div>


			</div>
        
        	
		
        
    </div>
    <div class="bottom-content">    
        <div id="metabox2" class="description">
            
            	<h2 class="metabox2-title"><?php _e('DESCRIPCION'); ?></h2>
        	<div class="text">
        	<?php global $post; echo wpautop(get_post_meta( get_the_ID(), '_cmb_descripcion', true ));?>
        	</div>
        	</div>
     </div>   
<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<!-- Ends #single -->

</div>
</div>

<?php get_footer ()?>
