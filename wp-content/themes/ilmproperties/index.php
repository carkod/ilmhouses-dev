<?php get_header(); ?>

<?php include(TEMPLATEPATH . '/searchform.php'); ?>
<div class="archives">
<?php if (have_posts()) :  ?>



<ul class="item">
<?php while (have_posts()) : the_post(); ?>
    	
	
		<li><a href="<?php the_permalink (); ?>" title="<?php _e('Ver Propiedad'); ?>"><?php the_post_thumbnail();  ?><?php echo '<div class="archives-title">' ; the_title (); ?>, <?php $terms = get_the_terms( $post->ID, 'ciudades' ); if ( $terms && ! is_wp_error( $terms ) ) : $ciudades_links = array();foreach ( $terms as $term ) {$ciudades_links[] = $term->name;}	$on_ciudades = join( ", ", $ciudades_links ); echo $on_ciudades; ?>, <?php global $post; echo get_post_meta( get_the_ID(), '_cmb_precio', true ); echo '&nbsp;€</div>'; endif; ?></a>
		
          </li> 
          
          

		<?php endwhile; ?> 
        
</ul> 

<?php 

// Pagination with fixed structure

if ( function_exists('wp_pagenavi') )
{
	ob_start();
	wp_pagenavi();

	$pagenavi = ob_get_contents();
	ob_end_clean();

	if ( !strstr($pagenavi, 'previouspostslink') ) $pagenavi = str_replace('<span', '<span class="previouspostslink">&lt; ANTERIOR</span>'."\r\t".'<span', $pagenavi);
	if ( !strstr($pagenavi, 'nextpostslink') ) $pagenavi = str_replace('</div>', '<span class="nextpostslink">SIGUIENTE &gt;</span>'."\r".'</div>', $pagenavi);

	echo $pagenavi;
}

?>

   	
<?php else : ?> 
<div class="notfound">
<h2>No encontramos las propiedades que busca. </h2>
<p>En nuestra página web tenemos sólo una pequeña muestra de nuestro catálogo general de propiedades inmobiliarias. Si busca una propiedad en una zona de España concrta o de unas características que no encuentra en la web, póngase en contacto con nosotros y le haremos llegar información de lo que está buscando, bien sean viviendas, parcelas, oficinas, locales comerciales, etc.</p>
</div>
	<?php endif; ?>
    
</div>
<?php //end of container1 ?> 
<?php get_footer(); ?>

