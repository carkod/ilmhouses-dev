<div id="searchfield">
<form method="get" id="searchform" action="" />
<input type="text" placeholder="<?php esc_attr_e( 'Use topics, tags, dates and titles.', 'carkod' ); ?>" name="s" id="s" size="40" />
<input type="submit" id="searchsubmit" value="Search" class="btn" />

</form>

<?php $customized = array('post_type' => 'customized','posts_per_page' => 1, ); $the_query = new WP_Query( $customized ); while ( $the_query->have_posts() ) :$the_query->the_post(); the_content() ; endwhile; wp_reset_postdata(); ?>

</div>

