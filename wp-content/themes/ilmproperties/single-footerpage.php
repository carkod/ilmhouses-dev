<?php get_header(); ?>

<?php if ( have_posts() ) :  ?>
<div id="page">
<?php while ( have_posts() ) : the_post(); ?>



		<h2 class="page-title"><?php the_title (); ?></h2>					
		<?php the_content (); ?>
            
		<?php endwhile; ?> 
        

    <?php else: ?>
		<p><?php _e('Sorry, I could not find what you are looking for.'); ?></p>

</div>
        
	<?php endif; ?>


<?php get_footer(); ?>

